#include <assert.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define EDGES_LIMIT 6000
#define VERTICES_LIMIT 2000

#define BITMAP_SIZE(n) (((n) + 7) / 8)

typedef unsigned char bitmap_t;

struct tree
{
	struct list
	{
		unsigned value;
		unsigned count;
		struct list *next;
	} *value;

	enum label {Leaf = 1, Prime, Complete} label;

	struct tree *next;
	struct tree *child;
};

// Memory pool for storing the decomposition tree.
static struct
{
	struct list nodes[VERTICES_LIMIT * VERTICES_LIMIT * 2];
	size_t nodes_count;

	struct list *lists; // linked list of recycled nodes

	struct tree trees[VERTICES_LIMIT * VERTICES_LIMIT];
	size_t trees_count;
} memory;

struct induced
{
	size_t count; // number of classes in the partition
	size_t first[EDGES_LIMIT]; // position of the first edge in each partition class
	size_t length[EDGES_LIMIT]; // number of edges in each partition class
};

struct hypergraph
{
	size_t vertices_count;
	size_t edges_count;

	bitmap_t vertices[VERTICES_LIMIT][BITMAP_SIZE(EDGES_LIMIT)];
	bitmap_t edges[EDGES_LIMIT][BITMAP_SIZE(VERTICES_LIMIT)];

	unsigned vertex_index[VERTICES_LIMIT]; // map: position in sorting -> index in array
	unsigned vertex_position[VERTICES_LIMIT]; // map: index in array -> position in sorting

	unsigned edge_index[EDGES_LIMIT]; // map: position in sorting -> index in array

	struct
	{
		size_t list[EDGES_LIMIT];
		size_t count;
	} vertex_edges[VERTICES_LIMIT]; // list of edges in each vertex

	struct
	{
		size_t list[VERTICES_LIMIT];
		size_t count;
	} edge_vertices[EDGES_LIMIT]; // list of vertices in each edge
};

struct vertices
{
	bitmap_t list[BITMAP_SIZE(VERTICES_LIMIT)];
};

static inline void bitmap_set(bitmap_t *restrict bitmap, unsigned index, bool value)
{
    unsigned mask = (1 << (index % 8));
    bitmap[index / 8] = (bitmap[index / 8] & ~mask) | (mask * value);
}

static inline bool bitmap_get(const bitmap_t *restrict bitmap, unsigned index)
{
    return bitmap[index / 8] & (1 << (index % 8));
}

static struct list *list_new(struct list *restrict next, unsigned value)
{
	struct list *result;
	if (memory.lists)
	{
		result = memory.lists;
		memory.lists = memory.lists->next;
	}
	else
	{
		assert(memory.nodes_count < sizeof(memory.nodes) / sizeof(*memory.nodes)); // memory limit reached (modules)
		result = memory.nodes + memory.nodes_count++;
	}

	result->count = (next ? next->count + 1 : 1);
	result->next = next;
	result->value = value;
	return result;
}

static void list_recycle(struct list *restrict list)
{
	struct list *first = list;
	while (list->next)
		list = list->next;
	list->next = memory.lists;
	memory.lists = first;
}

static struct tree *tree_new(struct tree *restrict next, struct tree *restrict child, struct list *restrict value, enum label label)
{
	assert(memory.trees_count < sizeof(memory.trees) / sizeof(*memory.trees)); // memory limit reached (trees)

	struct tree *result = memory.trees + memory.trees_count++;
	result->next = next;
	result->child = child;
	result->value = value;
	result->label = label;
	return result;
}

// Swaps two vertices in the hypergraph.
static void swap(unsigned *restrict indices, unsigned *restrict positions, size_t pos_a, size_t pos_b)
{
	unsigned swap;

	swap = indices[pos_a];
	indices[pos_a] = indices[pos_b];
	indices[pos_b] = swap;

	swap = positions[indices[pos_b]];
	positions[indices[pos_b]] = positions[indices[pos_a]];
	positions[indices[pos_a]] = swap;
}

// Sorts a list of modules lexicographically.
static size_t modules_sort(const struct hypergraph *restrict H, struct list **restrict M, size_t total)
{
	struct class
	{
		unsigned index;
		bool done;
		struct class *next;
	};

	struct class classes[VERTICES_LIMIT] = {0};
	size_t classes_count = 0;
	size_t module_class[VERTICES_LIMIT] = {0};

	struct class *split[VERTICES_LIMIT];
	size_t split_count;

	struct list *module_element[VERTICES_LIMIT]; // element in each list which we are comparing

	size_t i, j;

	// Split modules into classes according to length.
	i = 0;
	do
	{
		if (classes_count)
			classes[classes_count - 1].next = classes + classes_count;
		classes[classes_count].next = 0;
		classes_count += 1;

		module_class[i] = classes_count - 1;
		module_element[i] = M[i];

		// Find the last list in M with the same number of elements.
		size_t count = M[i]->count;
		for(j = i + 1; j < total; j += 1)
		{
			if (M[j]->count != count)
				break;

			module_class[j] = classes_count - 1;
			module_element[j] = M[j];
		}

		i = j;
	} while (i < total);

	struct class *class;
	size_t list;

	for(i = 0; i < H->vertices_count; i += 1)
	{
		// Determine which classes need to be split.
		// Remember if there are both elements with and without vertex i present.
		bool present[VERTICES_LIMIT][2] = {0};
		for(list = 0; list < total; list += 1)
		{
			struct list *element = module_element[list];
			present[module_class[list]][(element && element->value == i)] = true;
		}

		split_count = 0;

		for(list = 0; list < total; list += 1)
		{
			struct list *element = module_element[list];

			if (element && element->value == i)
			{
				module_element[list] = element->next;

				if (!present[module_class[list]][0] || !present[module_class[list]][1])
					continue; // no split required

				class = classes + module_class[list];
				if (!class->done)
				{
					split[split_count++] = class;
					class->done = true;

					// Add new class between class and class->next.
					classes[classes_count].next = class->next;
					class->next = classes + classes_count;
					classes_count += 1;
				}

				module_class[list] = class->next - classes;
			}
		}

		for(j = 0; j < split_count; j += 1)
			split[j]->done = false;
	}

	// Assign position to each class.
	for(class = classes, list = 0; class; class = class->next, list += 1)
		class->index = list;

	// Reorder list.
	for(i = 0; i < total; i += 1)
		module_element[i] = M[i];
	for(list = 0; list < total; list += 1)
	{
		class = classes + module_class[list];
		if (class->done)
		{
			list_recycle(module_element[list]);
			continue;
		}
		M[class->index] = module_element[list];
		class->done = true;
	}

	return classes_count;
}

// Sorts hypergraph edges lexicographically.
static void edges_sort(struct hypergraph *restrict H, size_t start)
{
	struct class
	{
		unsigned index;
		bool split;
		struct class *next;
		size_t count;
	};

	struct class classes[EDGES_LIMIT] = {0};
	size_t classes_count = 1;
	classes[0].count = H->edges_count;

	size_t edge_class[EDGES_LIMIT] = {0};
	struct class *class;

	size_t edge, vertex;
	size_t i;

	unsigned present[EDGES_LIMIT] = {0};

	for(vertex = 0; vertex < H->vertices_count; vertex += 1)
	{
		struct class *split[EDGES_LIMIT];
		size_t split_count = 0;

		// Determine which classes need to be split.
		// Count how many edges in each class contain the vertex.
		for(i = 0; i < H->vertex_edges[H->vertex_index[vertex]].count; i += 1)
		{
			edge = H->vertex_edges[H->vertex_index[vertex]].list[i];
			present[edge_class[edge]] += 1;
		}

		for(i = 0; i < H->vertex_edges[H->vertex_index[vertex]].count; i += 1)
		{
			edge = H->vertex_edges[H->vertex_index[vertex]].list[i];
			class = classes + edge_class[edge];

			if (!present[edge_class[edge]] || (present[edge_class[edge]] == class->count))
			{
				present[edge_class[edge]] = 0;
				continue; // no split required
			}

			if (!class->split)
			{
				class->split = true;
				split[split_count++] = class;

				// Add new class between class and class->next.
				classes[classes_count].next = class->next;
				class->next = classes + classes_count;
				classes_count += 1;

				class->next->count = present[edge_class[edge]];
			}

			edge_class[edge] = class->next - classes;
		}

		// Restore state of helper variables.
		for(i = 0; i < split_count; i += 1)
		{
			split[i]->split = false;
			split[i]->count -= present[split[i] - classes];
			present[split[i] - classes] = 0;
		}
	}

	// Assign position to each class.
	assert(classes_count == H->edges_count);
	for(class = classes, edge = 0; class; class = class->next, edge += 1)
		class->index = edge;

	// Store edge positions.
	for(edge = 0; edge < H->edges_count; edge += 1)
		H->edge_index[classes[edge_class[edge]].index] = edge;
}

// Compares two edges lexicographically, excluding the vertices in minus.
static int cmp(const struct hypergraph *restrict H, size_t a, size_t b, const bitmap_t *restrict minus)
{
	size_t ai = 0, bi = 0;
	int a_smallest = -1, b_smallest = -1; // smallest vertex by the ordering present in one edge but not in the other

	while (1)
	{
		int av, bv;

		// Take a vertex from edge a.
		for(av = -1; ai < H->edge_vertices[a].count; ai += 1)
		{
			size_t i = H->edge_vertices[a].list[ai];
			if (!bitmap_get(minus, i))
			{
				av = i;
				break;
			}
		}

		// Take a vertex from edge b.
		for(bv = -1; bi < H->edge_vertices[b].count; bi += 1)
		{
			size_t i = H->edge_vertices[b].list[bi];
			if (!bitmap_get(minus, i))
			{
				bv = i;
				break;
			}
		}

		if ((av < 0) && (bv < 0))
			break;

		if ((av < 0) || ((bv >= 0) && (bv < av))) // vertex bv not present in a
		{
			if ((b_smallest < 0) || (H->vertex_position[bv] < b_smallest))
				b_smallest = H->vertex_position[bv];
			bi += 1;
		}
		else if ((bv < 0) || ((av >= 0) && (av < bv))) // vertex av not present in b
		{
			if ((a_smallest < 0) || (H->vertex_position[av] < a_smallest))
				a_smallest = H->vertex_position[av];
			ai += 1;
		}
		else
		{
			assert(av == bv);
			ai += 1;
			bi += 1;
		}
	}

	if ((a_smallest < 0) && (b_smallest < 0))
		return 0; // a == b
	if ((a_smallest < 0) || ((b_smallest >= 0) && (b_smallest < a_smallest)))
		return -1; // a < b
	else
	{
		assert(a_smallest >= 0);
		assert((b_smallest < 0) || (a_smallest < b_smallest));
		return 1; // a > b
	}
}

// Checks if two edges differ by the vertices before vertices_count.
static bool diff(const struct hypergraph *restrict H, size_t a, size_t b, size_t vertices_count)
{
	size_t ai = 0, bi = 0;
	while (1)
	{
		int av, bv;

		for(av = -1; ai < H->edge_vertices[a].count; ai += 1)
		{
			size_t i = H->edge_vertices[a].list[ai];
			if (H->vertex_position[i] < vertices_count)
			{
				av = i;
				break;
			}
		}

		for(bv = -1; bi < H->edge_vertices[b].count; bi += 1)
		{
			size_t i = H->edge_vertices[b].list[bi];
			if (H->vertex_position[i] < vertices_count)
			{
				bv = i;
				break;
			}
		}

		if ((av < 0) && (bv < 0))
			return false;
		else if (av != bv)
			return true;

		ai += 1;
		bi += 1;
	}
}

static bool edge_empty(const struct hypergraph *restrict H, size_t edge, size_t start, size_t end)
{
	size_t vertex;
	for(vertex = 0; vertex < H->edge_vertices[edge].count; vertex += 1)
	{
		size_t index = H->vertex_position[H->edge_vertices[edge].list[vertex]];
		if ((start <= index) && (index < end))
			return false;
	}
	return true;
}

static size_t comparison(const struct hypergraph *restrict H, const struct induced *restrict induced, size_t current, const bitmap_t *restrict minus, size_t X[static VERTICES_LIMIT])
{
	size_t ei, fi;
	bool e_ends = false, f_ends = false;
	int sign;

	memset(X, 0, H->vertices_count * sizeof(*X));

	ei = induced->first[current];
	fi = induced->first[current + 1];
	while (1)
	{
		e_ends = (ei == induced->first[current] + induced->length[current]);
		f_ends = (fi == induced->first[current + 1] + induced->length[current + 1]);

		if (e_ends && f_ends)
			return 0;

		if (e_ends || f_ends)
			break;

		sign = cmp(H, H->edge_index[ei], H->edge_index[fi], minus);
		if (sign)
			break;

		ei += 1;
		fi += 1;
	}

	size_t min = 0;
	size_t i, j;
	size_t j_end;
	const bitmap_t *edge;

	if (f_ends || (sign < 0))
	{
		j = induced->first[current + 1];
		j_end = j + induced->length[current + 1];
		edge = H->edges[H->edge_index[ei]];
	}
	else
	{
		assert(e_ends || (sign > 0));

		j = induced->first[current];
		j_end = j + induced->length[current];	
		edge = H->edges[H->edge_index[fi]];
	}

	// Find the smallest symmetric difference.
	do
	{
		size_t temp[VERTICES_LIMIT];
		size_t count = 0;

		for(i = 0; i < H->vertices_count; i += 1)
		{
			if (bitmap_get(minus, i))
				continue;
			if (bitmap_get(edge, i) != bitmap_get(H->edges[H->edge_index[j]], i))
				temp[count++] = i;
		}

		if (!min || (count && (count < min)))
		{
			memcpy(X, temp, count * sizeof(*X));
			min = count;
		}

		j += 1;
	} while (j < j_end);

	assert(min);
	return min;
}

static void induced_init(const struct hypergraph *restrict H, struct induced *restrict induced, size_t start, size_t C_size)
{
	size_t edge;

	// Find edges of the hypergraph induced by C.
	// Partition the edges into classes.
	induced->count = start;
	for(edge = (start ? induced->first[start] : 0); edge < H->edges_count; edge += 1)
	{
		if (induced->count > start)
		{
			if (!diff(H, H->edge_index[edge], H->edge_index[induced->first[induced->count - 1]], C_size))
			{
				induced->length[induced->count - 1] += 1;
				continue;
			}
		}

		if (edge_empty(H, H->edge_index[edge], 0, C_size))
			continue;

		// Add current edge to the induced hypergraph.
		induced->first[induced->count] = edge;
		induced->length[induced->count] = 1;
		induced->count += 1;
		assert(induced->count <= H->edges_count);
	}
}

static struct list *minimal_module(struct hypergraph *restrict H, unsigned i0, unsigned i1)
{
	size_t edge, vertex;

	struct vertices C = {0};
	bitmap_set(C.list, i0, true);
	bitmap_set(C.list, i1, true);

	for(vertex = 0; vertex < H->vertices_count; vertex += 1)
	{
		H->vertex_index[vertex] = vertex;
		H->vertex_position[vertex] = vertex;
	}

	// Move i1 and i0 to the start of the vertices list.
	swap(H->vertex_index, H->vertex_position, i1, 1);
	swap(H->vertex_index, H->vertex_position, H->vertex_position[i0], 0);

	edges_sort(H, 0);

	size_t C_size = 2;

	struct induced induced;
	induced_init(H, &induced, 0, C_size);

	if (induced.count == 1)
		goto finally;

	size_t L = 0;
	while (L + 1 < induced.count)
	{
		size_t X[VERTICES_LIMIT];
		size_t count = comparison(H, &induced, L, C.list, X);

		if (count)
		{
			// Update C and the sorting.
			for(vertex = 0; vertex < count; vertex += 1)
			{
				bitmap_set(C.list, X[vertex], true);
				swap(H->vertex_index, H->vertex_position, C_size, H->vertex_position[X[vertex]]);
				C_size += 1;
			}
			edges_sort(H, C_size - count);

			// Refine partition with the vertices in X.
			L = 0;
			induced_init(H, &induced, L, C_size);
			continue;
		}

		L += 1;
	}

finally:
	{
		struct list *result = 0;
		for(vertex = H->vertices_count; vertex--;)
			if (bitmap_get(C.list, vertex))
				result = list_new(result, vertex);
		return result;
	}
}

static bool overlap(const struct list *restrict a, const struct list *restrict b)
{
	bool a_minus_b = false, b_minus_a = false, a_and_b = false;

	while (a && b)
	{
		if (a->value < b->value)
		{
			a_minus_b = true;
			a = a->next;
		}
		else if (a->value > b->value)
		{
			b_minus_a = true;
			b = b->next;
		}
		else
		{
			a_and_b = true;
			a = a->next;
			b = b->next;
		}

		if (a_minus_b && b_minus_a && a_and_b)
			return true;
	}
	if (a)
		a_minus_b = true;
	else if (b)
		b_minus_a = true;

	return (a_minus_b && b_minus_a && a_and_b);
}

// Merges two sorted lists of values.
static void modules_merge(struct list **restrict module, struct list *restrict other)
{
	struct list **first = module;
	size_t count = 0;

	while (1)
	{
		if ((*module)->value > other->value)
		{
			struct list *next = other->next;
			other->next = *module;
			*module = other;
			other = next;
		}
		else if ((*module)->value == other->value)
		{
			struct list *next = other->next;
			other->next = 0;
			list_recycle(other);
			other = next;
		}

		module = &(*module)->next;
		count += 1;
		if (!*module)
		{
			*module = other;
			if (other)
				count += other->count;
			break;
		}
		if (!other)
		{
			count += (*module)->count;
			break;
		}
	}

	do
	{
		(*first)->count = count;
		first = &(*first)->next;
		count -= 1;
	} while (*first);
}

static bool list_eq(struct list *restrict a, struct list *restrict b)
{
	assert(a);
	assert(b);

	if (a->count != b->count)
		return false;

	do
	{
		if (a->value != b->value)
			return false;
		a = a->next;
		b = b->next;
	} while (a && b);

	return (a == b);
}

/*
static size_t tree_health(const struct tree *restrict tree)
{
	size_t expected, count;
	unsigned value;

	struct list *list = tree->value;
	assert(list); // invalid tree value
	count = expected = list->count;
	value = list->value;
	while (list = list->next)
	{
		expected -= 1;
		assert(list->count == expected); // invalid value count
		assert(list->value > value); // invalid value
		value = list->value;
	}
	assert(!--expected); // tree value too short

	if (tree->child)
		assert(count >= tree_health(tree->child)); // tree child count wrong
	else
		assert(count == 1); // tree leaf is not a leaf

	if (tree->next)
		count += tree_health(tree->next);
	return count;
}
*/

// Finds the tree path in the modular decomposition tree from the root to pivot.
static struct tree *find(struct hypergraph *restrict H, unsigned pivot, bitmap_t *restrict done)
{
	size_t i;
	struct list *M[VERTICES_LIMIT];
	struct list *modules[VERTICES_LIMIT - 1];
	size_t cardinalities[VERTICES_LIMIT + 1] = {0};
	size_t M_count;

	bitmap_set(done, pivot, true);

	static unsigned step = 0;
	step += 1;
	fprintf(stderr, "Pivot step %u\n", step);

	// Calculate minimal modules and sort them by size.
	M_count = 0;
	cardinalities[1] = 1;
	for(i = 0; i < H->vertices_count; i += 1)
	{
		if (i == pivot)
			continue;

		modules[M_count] = minimal_module(H, pivot, i);
		cardinalities[modules[M_count]->count] += 1;
		M_count += 1;
	}
	for(i = 1; i <= H->vertices_count; i += 1)
		cardinalities[i] += cardinalities[i - 1];
	M[0] = list_new(0, pivot);
	for(i = 0; i < M_count; i += 1)
	{
		size_t index = modules[i]->count;
		cardinalities[index] -= 1;
		M[cardinalities[index]] = modules[i];
	}
	M_count += 1;

	// Sort modules lexicographically while removing duplicates.
	M_count = modules_sort(H, M, M_count);

	// Merge overlapping modules and remove duplicates again.
	enum label labels[VERTICES_LIMIT] = {0};
	size_t last = 0;
	for(i = 1; i < M_count; i += 1)
	{
		if (overlap(M[last], M[i]))
		{
			labels[last] = Complete;
			labels[i] = Complete;

			modules_merge(&M[last], M[i]);
			M[i] = 0;
		}
		else if (list_eq(M[last], M[i]))
		{
			list_recycle(M[i]);
			M[i] = 0;
		}
		else
			last = i;
	}
	size_t position = 1;
	for(i = 1; i < M_count; i += 1)
	{
		if (M[i])
		{
			if (position < i)
				M[position] = M[i];
			position += 1;
		}
	}
	M_count = position;

	// Generate tree path from the modules.
	struct tree *tree = tree_new(0, 0, M[0], Leaf);
	for(i = 1; i < M_count; i += 1)
		tree = tree_new(0, tree, M[i], (labels[i] ? Complete : Prime));

	return tree;
}

// Completes a tree by adding subtrees for to paths that haven't been calculated yet.
static void tree_complete(struct hypergraph *restrict H, struct tree *restrict tree, bitmap_t *restrict done)
{
	if (tree->label == Leaf)
		return;

	tree_complete(H, tree->child, done);

	struct list *list;
	for(list = tree->value; list; list = list->next)
	{
		if (bitmap_get(done, list->value))
			continue; // subtree already attached

		struct tree *new = find(H, list->value, done);
		while (new->value->count >= tree->value->count)
		{
			list_recycle(new->value);
			new = new->child;
		}
		tree_complete(H, new, done);
		new->next = tree->child;
		tree->child = new;
	}
}

// incidence must be NUL-terminated and use line feed as vertex terminator.
static void hypergraph_read(struct hypergraph *restrict hypergraph, const char *restrict filename)
{
	size_t vertex, edge;

	void *buffer;
	struct stat info;

	int fd = open(filename, O_RDONLY);
	if (fd < 0)
	{
		printf("unable to open file\n");
		abort();
	}
	if (fstat(fd, &info) < 0)
	{
		close(fd);
		printf("unable to stat file\n");
		abort();
	}

	buffer = mmap(0, info.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);
	if (buffer == MAP_FAILED)
	{
		printf("unable to mmap file\n");
		abort();
	}

	const char *incidence = buffer;
	size_t size = info.st_size;

	const char *end = memchr(incidence, '\n', size);
	if (!end)
	{
		printf("invalid hypergraph at line 1\n");
		abort();
	}

	*hypergraph = (struct hypergraph){0};
	hypergraph->vertices_count = 0; // will be initialized in the loop
	hypergraph->edges_count = end - incidence;

	if (hypergraph->edges_count > EDGES_LIMIT)
	{
		printf("hypergraph too big (edges)\n");
		abort();
	}

	do
	{
		vertex = hypergraph->vertices_count;
		hypergraph->vertices_count += 1;

		if (size < hypergraph->edges_count + 1)
		{
			printf("invalid hypergraph at line %u\n", (unsigned)hypergraph->vertices_count);
			abort();
		}

		if (hypergraph->vertices_count > VERTICES_LIMIT)
		{
			printf("hypergraph too big (vertices)\n");
			abort();
		}

		for(edge = 0; edge < hypergraph->edges_count; edge += 1)
		{
			switch (incidence[edge])
			{
			case '1':
				bitmap_set(hypergraph->vertices[vertex], edge, true);
				bitmap_set(hypergraph->edges[edge], vertex, true);

				hypergraph->vertex_edges[vertex].list[hypergraph->vertex_edges[vertex].count++] = edge;
				hypergraph->edge_vertices[edge].list[hypergraph->edge_vertices[edge].count++] = vertex;
			case '0':
				break;

			default:
				printf("invalid hypergraph at line %u\n", (unsigned)hypergraph->vertices_count);
				abort();
			}
		}
		if (incidence[hypergraph->edges_count] != '\n')
		{
			printf("invalid hypergraph at line %u\n", (unsigned)hypergraph->vertices_count);
			abort();
		}

		incidence += hypergraph->edges_count + 1;
		size -= hypergraph->edges_count + 1;
	} while (size);

	munmap(buffer, info.st_size);
}

static void print_module(const struct list *restrict module)
{
	for(; module; module = module->next)
		printf("%u ", module->value);
	printf("\n");
}

static void print_tree(const struct tree *restrict tree, unsigned padding)
{
	static const char *strings[] = {[Leaf] = "leaf", [Prime] = "prime", [Complete] = "complete"};
	do
	{
		if (padding)
		{
			size_t i;
			for(i = 0; i < padding - 1; i += 1)
				printf("  ");
			printf("|-");
		}
		printf("%s: ", strings[tree->label]);

		if (tree->label == Leaf)
			printf("%u\n", tree->value->value);
		else
		{
			struct list *list;
			for(list = tree->value; list; list = list->next)
				printf("%u ", list->value);
			printf("\n");
			print_tree(tree->child, padding + 1);
		}
	} while (tree = tree->next);
}

static void tree_recycle(struct tree *restrict tree)
{
	// WARNING: Doesn't recycle tree nodes (just lists of values).
	do
	{
		if (tree->child)
			tree_recycle(tree->child);
		list_recycle(tree->value);
	} while (tree = tree->next);
}

int main(int argc, char *argv[])
{
	static struct hypergraph hypergraph;

	if (argc != 2)
	{
		fprintf(stderr, "usage: modules <filename>\n");
		return 0;
	}

#if defined(MODULES)
	size_t i, j;
	hypergraph_read(&hypergraph, argv[1]);

	for(i = 0; i + 1 < hypergraph.vertices_count; i += 1)
	{
		for(j = i + 1; j < hypergraph.vertices_count; j += 1)
		{
			memory.nodes_count = 0;

			printf("%u %u: ", (unsigned)i, (unsigned)j);
			print_module(minimal_module(&hypergraph, i, j));
		}
	}
	printf("\n");
#endif

	memory.nodes_count = 0;
	memory.trees_count = 0;

	hypergraph_read(&hypergraph, argv[1]);

	size_t pivot = 0;
	bitmap_t done[BITMAP_SIZE(VERTICES_LIMIT)] = {0};
	struct tree *tree = find(&hypergraph, pivot, done);
	tree_complete(&hypergraph, tree, done);

	print_tree(tree, 0);

/*
tree_recycle(tree);
struct list *list = memory.lists;
size_t count = 0;
while (list)
{
	count += 1;
	list = list->next;
}
printf("count = %u;     allocated = %u\n", (unsigned)count, (unsigned)memory.nodes_count);
*/

	return 0;
}
