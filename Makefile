CFLAGS=-std=c99 -pedantic -O2 -fomit-frame-pointer -DNDEBUG
#CFLAGS=-std=c99 -pedantic -O0 -g -DNDEBUG
#CFLAGS=-std=c99 -pedantic -g

all: modules

modules: modules.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

clean:
	rm -f modules
	rm -f *.o
