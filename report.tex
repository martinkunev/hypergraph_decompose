\documentclass{article}

\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{natbib}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem{corollary}{Corollary}

\title{About Modular Decompositions of Hypergraphs}
\date{2019 June}
\author{Martin Kunev, University Paris Diderot}

\begin{document}

\maketitle

\tableofcontents
\clearpage

\section{Presentation of the paper}

This article presents the paper "A General Algorithmic Scheme for Modular Decompositions of Hypergraphs" \cite{paper} and adds to it some theoretical work and an implementation of an algorithm described in the paper.

\bigskip

Hypergraph is a generalization of the formalism of graph. In a graph each edge connects two vertices, while in a hypergraph a an arbitrary number of vertices can be connected by a hyperedge. Given a finite set of vertices, a hypergraph is a family of non-empty subsets of these vertices, with every vertex participating in some element of the family.

\begin{definition}
Given $V$ - a set of vertices, a hypergraph $H$ is a pair $(V, \mathcal{H})$, where $\mathcal{H} = \{e_0, e_1, ..., e_n\}$ is a family of sets such that $\emptyset \subset e_i \subseteq V$ and $V = \bigcup \mathcal{H}$.
\end{definition}

We denote the set of vertices of a given hypergraph $H$ with $V(H)$ and the set of hyperedges $\mathcal{E}(H)$. We are interested in particular in simple hypergraphs, which are hypergraphs where all hyperedges are distinct.
Each hypergraph can be represented as a bipartite graph, called incidence bipartite graph.

The two disjoint sets of vertices of the incidence bipartite graph are: (i) the vertices of $H$; (ii) the hyperedges of the hypergraph.

There is an edge between a hypergraph vertex and a hyperedge iff the hyperedge contains the vertex.

\begin{figure}[b]
  \includegraphics[width=340pt]{hypergraph_bipartite.png}
  \caption{Hypergraph matrix and its representation as an incidence bipartite graph}
  \label{fig:hypergraph_bipartite}
\end{figure}

\bigskip

\begin{definition}
Two non-empty sets $A$ and $B$ are said to overlap when $A \cap B \ne \emptyset$, $A \setminus B \ne \emptyset$ and $B \setminus A \ne \emptyset$. We denote as $A \not\perp B$.
\end{definition}

If to sets $A$ and $B$ do not overlap, we say that they are \textbf{orthogonal} and denote this $A \perp B$.

\begin{definition}
Given a hypergraph $H$ with edges $\mathcal{E}(H)$, an overlap graph of $\mathcal{E}(H)$ is a graph with vertices the elements of $\mathcal{E}(H)$ and edges connecting any two elements of $\mathcal{E}(H)$ that overlap.
Each connected component of this graph is called \textbf{overlap component}.
\end{definition}

In a graph, it is useful to define the concept of a module. Intuitively, a module is a set of vertices, which are indistinguishable by the way they are connected to the rest of the vertices in the graph. In the literature, this concept is generalized to hypergraphs in several different ways: standard hypergraph module \cite{def-standard}, k-subset hypergraph module \cite{def-ksubset} and Courcelle's module \cite{def-courcelle}.

\begin{figure}[h]
  \includegraphics[width=200pt]{graph_table.png}
  \caption{The set of vertices $\{a, b, c\}$ forms a graph module}
  \label{fig:graph_table}
\end{figure}

\begin{definition}
Given a hypergraph $H$, we say that $M \subseteq V(H)$ is a \textbf{standard module} (or just module) when $\forall A, B \in \mathcal{E}(H) \, (A \cap M \ne \emptyset, B \cap M \ne \emptyset) \implies (A \setminus M) \cup (B \cap M) \in \mathcal{E}(H)$.
\end{definition}

\begin{definition}
Given a hypergraph $H$, we say that $M \subseteq V(H)$ is a \textbf{Courcelle's module} when $\forall A \in \mathcal{E}(H) \, A \perp M$.
\end{definition}

\begin{definition}
Given a hypergraph $H$, we say that $M \subseteq V(H)$ is a \textbf{k-subset module} when for all $A, B \subseteq V(H)$ such that $2 \le |A|, |B| \le k, A \cap M \ne \emptyset, B \cap M \ne \emptyset, A \setminus M = B \setminus M \ne \emptyset$ then $A \in \mathcal{E}(H) \iff B \in \mathcal{E}(H)$.
\end{definition}

To better understand modules in a hypergraph, we are going to introduce the concept of a partitive family.

\begin{definition}
Given a set of vertices $V$, a family of subsets $F$ of $V$ is called \textbf{partitive} when the following conditions are met:
\end{definition}

(i) $\emptyset$, $V$ and all singletons $\{x\}$ for $x \in V$ belong to $F$.

(ii) For all $ A, B \in F$ that overlap ($A \not\perp B$), all of $A \cap B$, $A \cup B$, $A \setminus B$ and $A \triangle B$ are elements of $F$.

\bigskip

Partitive families are important for the algorithm for modular decomposition. Every partitive family admits a unique decomposition tree \cite{decomposition}. Moreover, each node in the decomposition tree belongs to one of three categories:

\textbf{leaf node} - singleton

\textbf{prime node} - the subgraph rooted at the node cannot be decomposed any further

\textbf{complete node} - the subgraph rooted at the node is either a clique or an independent set

The modules of every hypergraph are a partitive family and thus every hypergraph can be uniquely decomposed into a tree where each node represents a module. Leaves represent singletons of the vertices in the hypergraph and all other nodes represents the union of their children.

\begin{figure}[h]
  \includegraphics[width=350pt]{decomposition.png}
  \caption{A matrix of a hypergraph and its modular decomposition tree}
  \label{fig:decomposition}
\end{figure}

One main goal of the paper is providing an algorithm for computing the decomposition tree of a simple hypergraph.

\clearpage

\section{Applications of modular decomposition of hypergraphs}

Finding the modular decomposition of a hypergraph is of interest not only due to its inherent theoretical implications, but also for its applications.

Many relationships in Biology are usually modelled as bipartite graphs. These include gene sharing between species, enzyme-reaction links in metabolic pathways, polination networks, gene–disease associations, etc.
The hypergraph formalism is equivalent to the bipartite one, but allows applying more tools, like modular decomposition. Exactly by using modular decomposition, important correlations in biological systems can be found.

In \cite{biology}, modular decomposition is used to classify viruses according to genetic similarities. Figure 4 shows a result from \cite{biology}, obtained by clustering. Clusters are an approximation of modules.

\begin{figure}[h]
  \includegraphics[width=320pt]{viruses.png}
  \caption{Clustering of genome - gene family relationship in viruses. Gray dots represent gene families and colored dots represent genomes.}
  \label{fig:viruses}
\end{figure}

\clearpage

\section{Theoretical work}

This section contains some interesting facts about hypermodules. Below is an original work introduced in this article.

\subsection{Twins}

\begin{definition}
Given a hypergraph $H$, we say that two vertices $a, b \in V(H)$ are \textbf{twins} when $\forall e \in \mathcal{E}(H) \, a \in e \iff b \in e$.
\end{definition}

In a graph, a set of twins is a module. Moreover, for bipartite graphs the only modules are the sets of twins and the connected components of the graph. The question that comes up is what can be said for twins in a hypergraph.

First we will consider the three definitions of hypergraph module and prove that a set of twins is a standard module, Courcelle's module and 2-subset module. Let $H$ be a hypergraph and $T = \{a, b\}$ be a set of twins.

\begin{theorem}
T is a Standard Hypergraph Module
\end{theorem}
Proof:

Let $A, B \in \mathcal{E}(H)$ and $A \cap T \ne \emptyset$, $B \cap T \ne \emptyset$.
Since $T = \{a, b\}$, without loss of generality let us assume $a \in A \cap T$. Then $a \in A$ and so $b \in A$.
We know that $B \cap T \ne \emptyset$.
If we assume that $a \in B \cap T$, then $a \in B$ and so $b \in B$ as well.
If we assume that $b \in B \cap T$, then $b \in B$ and so $a \in B$ as well.
In all cases $B \cap T = \{a, b\}$.
Then $A = (A \setminus T) \cup (B \cap T) \in \mathcal{E}(H)$.

\begin{theorem}
T is a Courcelle's module
\end{theorem}
Proof:

We will prove this by contradiction.
Suppose $A \in \mathcal{E}(H)$ and $A \not\perp T$.
So $A$ and $T$ overlap, which means $A \cap T \ne \emptyset$, $A \setminus T \ne \emptyset$, $T \setminus A \ne \emptyset$.
Since $T = \{a, b\}$, without loss of generality let us assume $a \in A \cap T$. Then $a \in A$ and so $b \in A$.
Then $T \setminus A = \{a, b\} \setminus A = \emptyset$ which is a contradiction.
We conclude that $A \perp T$.

\begin{theorem}
T is a 2-subset module
\end{theorem}
Proof:

Let $A, B \subseteq V(H)$ with $|A| = |B| = 2$ and $A \cap T \ne \emptyset$, $B \cap T \ne \emptyset$, $A \setminus T = B \setminus T \ne \emptyset$.
We need to prove that $A \in \mathcal{E}(H) \iff B \in \mathcal{E}(H)$. As we have made no assumptions about $A$ and $B$, the proof is the same in both directions of the equivalence.

We will prove the assumption that $A \in \mathcal{E}(H)$ leads to a contradiction. Suppose $A \in \mathcal{E}(H)$.
Since $T = \{a, b\}$, without loss of generality let us assume $a \in A \cap T$. Then $a \in A$.
Then, by assumption $b \in A$, but $|A| \le 2$ so $A = T$ and $A \setminus T = \emptyset$. This is a contradiction. So $A \not\in \mathcal{E}(H)$.

The implication $A \in \mathcal{E}(H) \implies B \in \mathcal{E}(H)$ is vacuously true. The same reasoning applies for the implication in the other direction.
We conclude that $T$ is a 2-subset module.

\bigskip

In the general case, it is not true that $T$ is a k-subset module.
In particular for $k \ge 3$ it is sufficient to take the hypergraph with $V(H) = \{a, b, c\}$ and a single edge $\mathcal{E}(H) = \{\{a, b, c\}\}$. Clearly $a$ and $b$ are twins.
However $A = \{a, b, c\}$, $B = \{a, c\}$ satisfy the properties $2 \le |A|, |B| \le k$, $A \cap T = \{a, b\}$, $B \cap T = \{a\}$ and $A \setminus T = B \setminus T = \{c\}$.
In this hypergraph $B$ is not an edge so $T$ is not a k-subset module.

\bigskip

\begin{corollary}
For any set $T$ of twins, $T$ is a standard module, Courcelle's module and 2-subset module.
\end{corollary}
Proof:

Any pair of twins in the set forms a module. Since the family of all modules is partitive, the union of any two overlapping pairs of twins is also a module. Thus, the theorems for two twins above are true for any number of twins.

\subsection{Overlap components}

In a graph, each overlap component of edges forms a connected component and is thus a graph module. We are interested in what can be said about hypergraph overlap components. Next we will prove that an overlap component of hyperedges is a Courcelle's module and a 2-subset module.

\begin{lemma}
If $C$ is an overlap component of a hypergraph $H$ with $a \in \mathcal{E}(H)$ and $c \in C$ such that $\bigcup C \setminus a \ne \emptyset$ and $c \subseteq a$. Then $a \in C$.
\end{lemma}
Proof:

Let $x \in \bigcup C \setminus a$ and so $x \in \bigcup C, x \not\in a$. Thus, there is some $c_0 \in C$ such that $x \in c_0$.
From the definition of overlap component, for some $p \in \mathbb{N}$ there exists a path of overlapping edges in $C$: $c_0, c_1, ..., c_{p}$ with $c_0 \not\perp c_1, ..., c_{p-1} \not\perp c_p$ and $c_p = c$.
We will prove that $a$ overlaps with $c_j$ for some $0 \le j \le p$.

We know that $c_0 \setminus a \ne \emptyset$ and also that $c = c_p \subseteq a$ so $c_p \setminus a = \emptyset$.
Let $i$ be the smallest number for which $c_i \setminus a = \emptyset$. Then $c_{i-1} \setminus a \ne \emptyset$.
We know that $c_{i-1} \not\perp c_i$ so $c_{i-1} \cap c_i \ne \emptyset$, $c_{i-1} \setminus c_i \ne \emptyset$, $c_i \setminus c_{i-1} \ne \emptyset$.
Since $c_i \setminus a = \emptyset$, it is also true that $c_i \subseteq a$. Then $c_{i-1} \cap a \ne \emptyset$.

As $c_i \setminus c_{i-1} \ne \emptyset$, let $y$ be such that $y \not\in c_{i-1}$ and $y \in c_i \subseteq a$. Thus $a \setminus c_{i-1} \ne \emptyset$.
Then the two edges $c_{i-1}$ and $a$ overlap and so $a \in C$.

\begin{theorem}
If $C$ is an overlap component of a hypergraph $H$, then $T = \bigcup C$ is a Courcelle's module
\end{theorem}
Proof:

If $\bigcup C = V(H)$, then $\forall A \in \mathcal{E}(H), A \setminus T = \emptyset$ so $T \perp A$. Thus $T$ is a module.

If $C$ is an overlap component such that $C = \{m\}$, $m \in \mathcal{E}(H)$, then $\forall A \in \mathcal{E}(H), A \perp T$. Thus, $T$ is a module.

The interesting case is when $C$ has at least two elements. We will prove by contradiction that in this case $T$ is a module.
Let $A \in \mathcal{E}(H)$ and suppose that $A \not\perp T$ which means $A \cap T \ne \emptyset$, $A \setminus T \ne \emptyset$, $T \setminus A \ne \emptyset$.
Since $A \setminus T \ne \emptyset$, we can conclude that $A \not\subseteq T$ and thus $A \not\in C$.

If we take $x \in A \cap T$, there exists $c \in C$ such that $x \in c$. So $A \cap c \ne \emptyset$. We also know that $A \setminus T \ne \emptyset$ so $A \setminus c \ne \emptyset$.
Since $A \not\in C$, $A$ and $c$ are orthogonal and thus we have $c \setminus A = \emptyset$. But then $c \subseteq A$.
Using that $T \setminus A \ne \emptyset$, we get from Lemma 1 that $A \in C$, which is a contradiction.
We can conclude that $A \perp T$ and so $T$ is a module.

\begin{theorem}
If $C$ is an overlap component of a hypergraph $H$, then $T = \bigcup C$ is a 2-subset module.
\end{theorem}

Let $A, B \subseteq V(H)$ with $|A| = 2, |B| = 2$ and $A \cap T \ne \emptyset$, $B \cap T \ne \emptyset$, $A \setminus T = B \setminus T \ne \emptyset$.
We need to prove that $A \in \mathcal{E}(H) \iff B \in \mathcal{E}(H)$. As we have made no assumptions about $A$ and $B$, the proof is the same in both directions of the implication.

We will calculate constraints about the size of various sets.
We know that $|A \setminus T| \ne \emptyset$ so $|A \setminus T| \ge 1$. We know that $|T| \ge 1$ and that $2 = |A| = |A \setminus T| + |T|$ so we can conclude that $|A \setminus T| = 1$, $|T| = 1$.
Since $A \cap T \ne \emptyset$, we have the constraint that $|A \cap T| \ge 1$ and thus $A \cap T = T$.
We get that $A = (A \setminus T) \cup (A \cap T) = (A \setminus T) \cup T$. Analogically $B = (B \setminus T) \cup T$.
But $(A \setminus T) = (B \setminus T)$ and thus $A = B$.

\bigskip

\textbf{The case of a k-subset module for $k > 2$}

An overlap component in a hypergraph is not necessarily a k-module for $k > 2$. To illustrate this, consider the hypergraph with vertices $\{a, b, c\}$ and edges $\{\{a, b, c\}, \{b, c\}\}$.
In this graph $T = \{b, c\}$ is the support of an overlap component, but $A = \{a, b, c\}, B = \{a, b\}$ satisfy the conditions for a k-module, while only $A$ is an edge.

\bigskip

\textbf{The case of a standard module}

An overlap component in a hypergraph is not necessarily a standard hypergraph module. This is illustrated by Figure 5 - $\{C, D\}$ form an overlap component, but taking the edges $A$ and $B$ shows that the support of the component is not a module.

\begin{figure}[b]
  \includegraphics[width=350pt]{overlap_standard_module.png}
  \caption{the overlap component $C\cup{D}$ is not a Standard Hypergraph Module}
  \label{standard_module}
\end{figure}

\clearpage

\section{An algorithm for modular decomposition of hypergraphs}

The goal is, for a given graph, to find its modular decomposition tree. It is convenient to represent a hypergraph as a matrix where each row is a vertex and each column a hyperedge. With this representation, a lexicographic ordering of the edges is an ordering of the columns of the matrix. The family of modules of the hypergraph is partitive, so for any set of vertices there is a unique minimal module containing them and thus we can define a function returning this module.

We are going to use the concept of induced hypergraph. Given a hypergraph $H$, the induced hypergraph by a set of vertice $C \subseteq V(H)$ is the restriction of $H$ to that subset. This is, the vertices of the induced graph are $C$ and the edges are the restrictions of the edges in $H$ to the set $C$ (if that restriction is non-empty).

\subsection{General algorithm}

The following algorithm \cite{paper} is based finds the modular decomposition tree of a hypergraph:

\bigskip

\noindent \textbf{input}: hypergraph $H$ with vertices $V$. \\
\textbf{output}: modular decomposition tree of the hypergraph with each internal node labelled as prime or complete

\bigskip

\noindent add all singletons $\{x\}$ where $x \in V$ to a list $M$. \\
choose a vertex $x_0 \in V$. \\
\textbf{for each} $x \in V$, such that $x \ne x_0$: \\
\indent add $minimal\_module(\{x_0, x\})$ to $M$; remember that $x$ generated this module \\
sort $M$ by increasing size and remove duplicate elements \\
\textbf{for each} pair of consecutive elements in $M$: \\
\indent \textbf{if} the elements overlap: \\
\indent \indent merge them and label the result as "complete" \\
mark every unlabelled element of $M$ as "prime" \\
/* The non-leaf elements of $M$ form a path in the modular decomposition tree from $x_0$ to the root. Now we need to find the decomposition subtree for each node in the path. */ \\
\textbf{for each} non-leaf element $X_i \in M$: \\
\indent take the set of vertices $V_i$ that generated $X_i$ \\
\indent compute the hypergraph induced by $V_i$ \\
\indent recursively compute the modular decomposition of the induced hypergraph \\
\indent attach the resulting tree to $X_i$

\subsection{Finding a minimal module}

The function $minimal\_module$ finds the minimal module containing a set of vertices.

\bigskip

\noindent \textbf{input}: hypergraph $H$ with vertices $V$ and a set of vertices $W \subseteq V$. \\
\textbf{output}: the minimal module in $H$ containing $W$.

\bigskip

\noindent compute a lexicographic ordering of the edges of $H$ w.r.t an arbitrary ordering of $V$ \\
C := W \\
Compute the hypergraph induced by the set of vertices $C$ \\
\textbf{for each} edge $f_i$ in the induced hypergraph: \\
\indent take the edges $e_k \in H$ such that $e_k \cap C = f_i$ \\
\indent compute the restriction to $V \setminus C$ of all the edges $e_k$ \\
\indent store the restrictions of edges in $L_i$, preserving their relative order \\
Q := [$L_1$, $L_2$, ...] /* ordered partition of the edges */ \\
if $|Q| = 1$: \\
\indent \textbf{return} $C$. \\
\textbf{for each} pair $L_i$ and $L_{i + 1}$ of consecutive elements of $Q$: \\
\indent X := $comparison(L_i, L_{i + 1})$ \\
\indent \textbf{if} $X \ne \emptyset$: \\
\indent \indent /* $L_i$ and $L_{i + 1}$ differ */ \\
\indent \indent C := $C \cup X$ \\
\indent \indent update $Q$ for the new value of $C$ \\
\indent \indent $L_i$ has been split; continue iterating from the first part after the split \\
\textbf{return} C.

\subsection{Comparing lists of edges}

The function $comparison$ compares two lists of edges and if they are different, returns a set of vertices that need to be added to the module in the iterative process.

\bigskip

\noindent \textbf{input}: Lists of edges $L'$ and $L"$ \\
\textbf{output}: Set of vertices that must be contained within the minimal module

\noindent \textbf{if} $L'$ = $L"$: \\
\indent \textbf{return} $\emptyset$. \\
append $\emptyset$ elements as padding to the shorter of $L'$ and $L"$ until $|L'| = |L"|$ \\
find the first pair of edges $e \in L'$, $f \in L"$ such that $e \ne f$ \\
compare lexicographically $e$ and $f$. \\
\textbf{if} $e < f$ or $f = \emptyset$: \\
\indent find $f' \in L"$, $f' \ne e$ which minimizes the size of the symmetric difference: $|e \triangle f'|$ \\
\indent \textbf{return} $f'$. \\
\textbf{else}: /* $f < e$ or $e = \emptyset$ */ \\
\indent find $e' \in L'$, $e' \ne f$ which minimizes the size of the symmetric difference: $|f \triangle e'|$ \\
\indent \textbf{return} $e'$.

\clearpage

\section{Implementation of an algorithm for hypergraph modular decomposition}

The algorithm is implemented in the C programming language. We are going to discuss some implementation details for the efficiency of the algorithm.

The implementation developed as part of this article represents a hypergraph as two matrices - one which stores for all edges whether a given vertex is in the edge, and another one which stores for all vertices whether a given edge contains the vertex. Each matrix is implemented as an array of bitmaps.

It will be useful to reorder the vertices so we also store two maps: from index where the vertex is stored to its position after the reordering; from vertex position after the reordering to the index where the vertex is stored.

\subsection{General algorithm}

Sorting the minimal modules $M$ by increasing size can be done efficiently using a variant of counting sort. We know each set has at most $|V(H)|$ elements so we can iterate over $M$ and count the number of elements for any given size (store this in an array where the index is the size). By iterating over the counts array, we can calculate the offset at which the elements with a given size should start in the sorted array. Finally, we can populate the sorted array by iterating over $M$ once more, placing an element according to the offset and incrementing the offset (so that it points to the first unoccupied position for the given size).

To represent the decomposition tree, each node stores a pointer to a linked list of its children. The linked list is itself composed of nodes. This allows to efficiently add children to a node.

\subsection{Finding a minimal module}

As part of this function, we need to sort the edges of $H$ on multiple occasions. Sorting in this case is implemented as ordered partition refinement \cite{sorting}. To represent the sorted edges, it is sufficient to remember the index in the hypergraph at which each edge in the sorting is stored. It is useful to precalculate for each vertex the list of edges that contain it. Iterating over this list is required for the sorting.

The induced hypergraph is only used to initialize the lists $L_1, L_2, ...$. These two operations can be done together, which eliminates the need to store the induced hypergraph. When sorting the edges, we can order the vertices in $C$ first - this will ensure that edges with the same vertices in the induced hypergraph are next to each other in the sorted list of edges. Then to initialize a given element in the partition $L_i$, it is sufficient to remember the position of the first edge in the sorting and the number of edges in $L_i$. Thus, initializing the partition can be done simply by iterating over all edges of $H$.

\subsection{Comparing lists of edges}

Finding the first edges that differ between two lists $L'$ and $L"$ in the partition can be done by comparing each pair of edges, ignoring the differences in vertices in $C$. Once a difference is found, we need to iterate over the edges in one of the lists in order to find which element in that list minimizes the symmetric difference. No more efficient algorithm for this step was found by the paper.

\subsection{Complexity}

Let us consider a hypergraph with \textbf{n} vertices and \textbf{m} hyperedges and sum of the sizes of all hyperedges \textbf{l}. We are going to find the worst case asymptotic complexity of the algorithm.

Each call to $comparison$ needs to iterate over the lists of edges and potentially iterate over all vertices for all edges in one of the lists. Thus, the complexity of $comparison$ is $O(l)$. For $minimal\_module$, every time $comparison$ is called, at least one vertex is added to $C$ so the number of calls to $comparison$ is $O(n)$. Sorting all the edges in the hypergraph can be done in $O(l)$ as there are no isolated vertices ($l > n$) and no empty hyperedges ($l > m$). For the general algorithm, the initial sorting can be in $O(n)$ and the precalculations can be done in $O(n.m)$. The bottleneck is all the calls to $minimal\_module$ - a total of $O(n^2)$ calls are required.

Finally, the complexity of the whole algorithm is $O(n^3.l)$.

\bibliographystyle{plain}
\bibliography{report} 

\end{document}
